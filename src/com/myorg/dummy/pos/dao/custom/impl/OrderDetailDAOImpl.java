package com.myorg.dummy.pos.dao.custom.impl;


import com.myorg.dummy.pos.dao.CrudDAOImpl;
import com.myorg.dummy.pos.dao.custom.OrderDetailDAO;
import com.myorg.dummy.pos.entity.OrderDetail;
import com.myorg.dummy.pos.entity.OrderDetailPK;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailDAOImpl extends CrudDAOImpl<OrderDetail, OrderDetailPK> implements OrderDetailDAO {


}
