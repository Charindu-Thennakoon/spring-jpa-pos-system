package com.myorg.dummy.pos.dao.custom.impl;


import com.myorg.dummy.pos.dao.custom.QueryDAO;
import com.myorg.dummy.pos.entity.CustomEntity;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

@Component
public class QueryDAOImpl implements QueryDAO {

    private EntityManager entityManager;
    @Override
    public CustomEntity getOrderDetail(String orderId) throws Exception {
        try {
            return (CustomEntity) entityManager.createQuery("SELECT  o.id, c.name , o.date "
                 + "FROM lk.ijse.dep.pos.entity.Order o INNER JOIN com.myorg.dummy.pos.entity.Customer c WITH o.customerId = c.id WHERE o.id=?1")
                .setParameter(1,orderId).getSingleResult();
        } catch (NoResultException e) {
           return null;
        }

    }

    @Override
    public CustomEntity getOrderDetail2(String orderId) throws Exception {
        try {
            return (CustomEntity) entityManager.createQuery("SELECT NEW com.myorg.dummy.pos.entity.CustomEntity(c.id,c.name,o.id) FROM lk.ijse.dep.pos.entity.Order o INNER JOIN o.Customer c WHERE o.id=?1")
                .setParameter(1,orderId).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }

    @Override
    public void setEntityManger(EntityManager entityManager) {
        this.entityManager=entityManager;
    }
}
