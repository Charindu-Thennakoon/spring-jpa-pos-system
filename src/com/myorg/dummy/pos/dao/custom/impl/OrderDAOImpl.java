package com.myorg.dummy.pos.dao.custom.impl;


import com.myorg.dummy.pos.dao.CrudDAOImpl;
import com.myorg.dummy.pos.dao.custom.OrderDAO;
import com.myorg.dummy.pos.entity.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderDAOImpl extends CrudDAOImpl<Order,String> implements OrderDAO {

  public String getLastOrderId() throws Exception {
    List list = entityManager.createQuery("SELECT o.id FROM com.myorg.dummy.pos.entity.Order o ORDER BY id DESC").setMaxResults(1).getResultList();
    return list.size() > 0 ? (String) list.get(0) : null;
  }

}
