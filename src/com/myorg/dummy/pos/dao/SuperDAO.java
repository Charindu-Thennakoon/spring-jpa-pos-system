package com.myorg.dummy.pos.dao;

import javax.persistence.EntityManager;

public interface SuperDAO {

  void setEntityManger(EntityManager entityManger);
}
